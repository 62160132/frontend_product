module.exports = {
  publicPath: process.env.NODE_ENV === 'production'
    ? '/~cs62160132/learn_bootstrap/'
    : '/'
}
